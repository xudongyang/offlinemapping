offlineMapping
==============

Two version for basecalling onlineMapping and fqc offllineMapping

Run the command './make' in *nix environment, the program will generate a dir 
in current dir which named the current time, and this dir includes two dirs that 
are onlineMapping for basecall and offlineMapping for fqc, the others tar files
are pack file.

WHAT IS OFFLINEMAPPING?
-----------------------

OfflineMapping is a fast and user-friendly tool for zebra sequencing data
to mapping and generating html report.

OfflineMapping uses bwa-0.7.12 and samtools-0.1.19 which provides a very fast
method for mapping the sequencing reads to reference genomes and subsequent 
analysis. OfflineMapping extract data about mapping result through reading .sam 
files, it also obtain basecall QC data for final html report.

It is written in Python-2.7, Python is an interpreted language, meaning that
code is not compiled before being run.  Also you need to install Python 2.X if
you want to run it.


USAGE
-----

Basically you use OfflineMapping just like other Python scripts. To get a 
complete list of supported options type:

	python analysisAndMapping.py --help
	
OfflineMapping need 
  two necessary parameters:
	<fovAddr>  E:\test\X1001\L01\Intensities 
				(make sure this directory has a file named FileName.txt)
	<prefix>	X1001_L01   (slide_lane)

  and four optional parameters:
	[--bioFile bioFile]	  --bioFile  E:\test\bioFile.csv  set biochemical information file
	[--ref Ecoli.fa] set reference genomes, default is Ecoli.fa, you can set Human.fa
	[--PE]			set sequencing type, default is SE, you can set PE using --PE
	[--mapping]		set fastq mapping, default is not mapping, using --mapping to change
	
Example:

	python analysisAndMapping.py E:\test\X1001\L01\Intensities X1001_L01 
	### This means reference is Ecoli.fa, SE, and no mapping result in html report.
	
	python analysisAndMapping.py E:\test\X1001\L01\Intensities X1001_L01 --bioFile E:\test\bioFile.csv --ref Human.fa --PE --mapping
	### This means reference is Human.fa, PE, have a biochemical file and do mapping.

	
FILE MEANING
------------

analysisAndMapping.py   The main entrance for this software

qcReport.py             Read qc.txt and extract qc info

fqStatReport.py         Read fqStat.txt and extract fq stat info

samStatReport.py        Read mapInfo.tsv and extract map info

splitSamFile.py         Read sam file and split into mapInfo.tsv

utility.py              Several utility function

fq2sam.py               Input a fastq and generate .sam .bam or other files

cat.py                  Including several simple class the cat qc.txt fqStat.txt mapInfo.tsv to single file

heatmapReport.py        Generate heatmap and bestfov report

summaryReport.py        Generate summary report

geneRealTimeReport.py geneSingleCycleHeatmap.py  Using for generating single cycle's report

mergeSingleCycleHeatmap.py  Generating all cycles's heatmap 

make                    Compile the release version by run ./make in linux cmd

common.ini              Control the mapping option like the addr of reference and bwa, samtools

offlineMapping_InstallManual.docx The usage of this software in doc version

python-2.7.9.amd64.msi  The python install package for x64 platform in windows

QCCriteria.json         Using for generateing a QC result csv for QCRun

version.txt             Including current release version

runTest.bat             A test bat file for this software, and you can see the report in the dir of test

README.md README.txt    Readme

SequencingInfo.csv      A test file for generate a table in summary report for Bio Info


DIR MEANING
------------

reference               Including reference data, now only have Ecoli.fa

software                Including software of bwa and samtools

templates               Including html templates for generateing reports

test                    Including test data 


offlineMapping 源代码管理与版本发布说明
---------------------------------------

1 说明
------
offlineMapping 是指根据下机的fastq文件以及其他信息生成易于交互的html报告的一个软件，运行在windows 64平台，有做mapping和不做mapping两种版本，软件名分别为offlineMapping onlineMapping

2 代码管理
----------
本软件的源代码管理使用Git，虽然最终发布版本是运行在win下，但是代码的编辑和版本发布都是在类unix 环境，因此建议将代码放在linux 上或者 win下安装cygwin

Git 简易使用说明(纯个人经验，高手勿喷)

生成新的仓库 git init 在当前路径下生成.git 目录，如果删掉它，则代码与git再无关系
与远程仓库建立连接  git remote add master https://github.com/***/***.git
下载远程仓库代码  git clone https://github.com/***/***.git
将远程仓库代码更新到本地仓库 git pull
提交代码到远程仓库 git push
修改代码之后添加修改  git add ***.py
提交修改到本地仓库  git commit –m “log”
查看状态  git status
查看修改  git diff
查看提交历史  git log
显示分支  git branch
切换分支  git checkout <branch>
新建分支  git branch <branch>
合并分支  git merge <branch>

3 版本发布
----------
在linux环境中，运行 ./make 即可看到当前目录新生成一个以当前时间命名的文件夹，里面有两个压缩文件和对应的文件夹，分别是onlineMapping offlineMapping, 当basecall发布新版本时，将onlineMapping放到basecall 的dev 里面，替换掉旧的，offlineMapping需要的时候发布，单独使用，与basecall版本无关。

每次有新的版本发布，如果有更改templates/*.html 的javascript代码：
1 先用源代码版本的offlineMapping 用测试数据生成一个**.summaryReport.html
2 使用这个html，用https://bgi-github.cgichina.cn/zhaofuxiang/reportTemplate 的程序生成新的html模板，具体使用方法，见其程序说明
3 ./make 发布新版本，记得测试通过之后再发布

4 程序流程说明
--------------
1 准备数据
数据下机，准备生成报告所需要的数据，即三大部分，qc文件，fastq文件，fastq统计文件

2 进行比对
对每个fastq文件，使用bwa samtools等工具进行比对到参考基因组，中间文件主要使用的是sam/bam 文件，通过分析sam/bam文件生成mapping统计文件

3 分别提取数据
从qc文件中提取basecall 信息，从 fastq统计文件中提取fastq信息，从mapping统计文件提取出mapping信息

4 生成最终报告
根据第3步获取的信息，分别生成summaryReport heatmapReport bestFovReport三个总报告，对于每个fastq文件生成一个包含fastq信息以及此fastq mapping信息的报告


5 其他部分
除了1-4 步的总体流程，该软件还有：
生成单个cycle的heatmap和report的功能，使用geneRealTimeReport.py
有针对QCRun生成的qc的csv结果的功能，analysisAndMapping.py 加参数 –c
其他琐碎功能待以后补充


