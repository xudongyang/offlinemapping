WHAT IS OFFLINEMAPPING?
-----------------------

OfflineMapping is a fast and user-friendly tool for zebra sequencing data
to mapping and generating html report.

OfflineMapping uses bwa-0.7.12 and samtools-0.1.19 which provides a very fast
method for mapping the sequencing reads to reference genomes and subsequent 
analysis. OfflineMapping extract data about mapping result through reading .sam 
files, it also obtain basecall QC data for final html report.

It is written in Python-2.7, Python is an interpreted language, meaning that
code is not compiled before being run.  Also you need to install Python 2.X if
you want to run it.


USAGE
-----

Basically you use OfflineMapping just like other Python scripts. To get a 
complete list of supported options type:

	python analysisAndMapping.py --help
	
OfflineMapping need 
  two necessary parameters:
	<fovAddr>  E:\test\X1001\L01\Intensities 
				(make sure this directory has a file named FileName.txt)
	<prefix>	X1001_L01   (slide_lane)

  and four optional parameters:
	[--bioFile bioFile]	  --bioFile  E:\test\bioFile.csv  set biochemical information file
	[--ref Ecoli.fa] set reference genomes, default is Ecoli.fa, you can set Human.fa
	[--PE]			set sequencing type, default is SE, you can set PE using --PE
	[--mapping]		set fastq mapping, default is not mapping, using --mapping to change
	
Example:

	python analysisAndMapping.py E:\test\X1001\L01\Intensities X1001_L01 
	### This means reference is Ecoli.fa, SE, and no mapping result in html report.
	
	python analysisAndMapping.py E:\test\X1001\L01\Intensities X1001_L01 --bioFile E:\test\bioFile.csv --ref 
Human.fa --PE --mapping
	### This means reference is Human.fa, PE, have a biochemical file and do mapping.
