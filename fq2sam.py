#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
### Py2.7 encode fix
reload(sys)
sys.setdefaultencoding('utf8')

import datetime
from ConfigParser import ConfigParser
import subprocess

###### Document Decription
''' 
    Applied a series of operator to fq, like align, map, sam2bam, sortbam, rmdup
'''

###### Version and Date
prog_version = '1.0.0'
prog_date = '2015-04-29'

###### Chang Log
''' 
    1.5.0   2016.08.18  delete Q40 and change GRR to ESR
'''

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <input.fq> <prefix> <outPutDir> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class Fq2Sam(object):
    """docstring for mappingPipeline"""
    def __init__(self, fq, prefix, outDir, reference, configFile=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'common.ini'), verbose=False):
        self.fq = os.path.abspath(fq)
        self.prefix = prefix
        if not outDir:
            self.outDir = os.path.dirname(self.fq)
        else:
            self.outDir = outDir
        self.configFile = configFile
        self.section = 'mapping'
        self.readConfig()

        ### Judge is exists the tmpDir
        if not os.path.exists(self.tmpDir):
            os.mkdir(self.tmpDir)
            
        self.platform = sys.platform
        self.verbose = verbose
        self.reference = reference

    def _log(self, content, showTime=True):
        ##### Display text to defined stream, add timestamp
        logStream = sys.stdout
        if isinstance(content, list):
            modifText = " ".join(content)
        else:
            modifText = str(content)

        if showTime == True:
            modifText += "    " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if self.verbose == True:
            logStream.write(modifText + "\n")
        else:
            return modifText

    def runSysCommand(self, command, showInLog=True, justTest=False):
        if showInLog == True:
            self._log(command, showTime=False)
            if justTest == True:
                return
        try:
            # returnVal = check_output(command, close_fds=True)
            returnVal = os.popen(" ".join(command))
        except Exception, e:
            raise e
        return returnVal

    def runSysCommandSubProcess(self, command, showInLog=True, justTest=False):
        if showInLog == True:
            self._log(command, showTime=False)
            if justTest == True:
                return
        try:
            p = subprocess.Popen(" ".join(command), stderr=subprocess.PIPE)
        except Exception, e:
            raise e
        return p.stderr.read()

    def _getOptPath(self, option):
        ##### Get a path option, convert to fixed style if on Windows
        exeDir = os.path.dirname(os.path.abspath(__file__))
        relPath = os.path.normcase(self.cfg.get(self.section, option))
        if relPath.startswith('.'):
            return os.path.abspath(os.path.join(exeDir, relPath))
        else:
            return os.path.abspath(relPath)

    def _removeTmpFile(self):
        removeList = [ self.bamFile, self.sortFile, self.sortBamFile, self.rmBamFile]
        if self.readType == 'SE':
            removeList.append(self.saiFile)
        elif self.readType == 'PE':
            removeList.append(self.saiFile1)
            removeList.append(self.saiFile2)


        if self.saveSam == False:
            removeList.append(self.samFile)

        for f in removeList:
            if os.path.exists(f):
                try:
                    os.remove(f)
                except Exception, e:
                    pass

    def _runAlignWinSE(self):
        self.saiFile = os.path.join(self.tmpDir, "%s.sai" % self.prefix)
        self.bamFile = os.path.join(self.tmpDir, "%s.bam" % self.prefix)
        self.sortFile = os.path.join(self.tmpDir, "%s.sort" % self.prefix)
        self.sortBamFile = os.path.join(self.tmpDir, "%s.sort.bam" % self.prefix)
        self.rmBamFile = os.path.join(self.tmpDir, "%s.sort.rm.bam" % self.prefix)

        ###### align sequence with bwa aln
        self._log("Begin to run bwa aln...")
        self.runSysCommand([self.bwa, 'aln', self.alnPara, self.ref, self.fq, '>%s' % self.saiFile])

        ##### convert result to sam format
        self._log("Begin to run bwa samse...")
        self.runSysCommand([self.bwa, 'samse', '-f', self.samFile, self.ref, self.saiFile, self.fq])

        ##### to get duplication, use samtools
        self._log("Begin to run samtools view...")
        self.runSysCommand([self.samtools, 'view', '-bS', self.samFile, '>', self.bamFile], showInLog=True)

        self._log("Begin to run samtools sort...")
        self.runSysCommand([self.samtools, 'sort', self.bamFile, self.sortFile])

        self._log("Begin to run samtools rmdup...")
        
        dupStr = self.runSysCommandSubProcess([self.samtools, 'rmdup', '-s', self.sortBamFile, self.rmBamFile])
        ### get duplication
        dupStr = dupStr.strip().split()
        try:
            self.dupRate = [dupStr[-9], dupStr[-7], dupStr[-5]]
        except Exception as e:
            #raise e 
            self.dupRate = '-'
        return 0

    def _runAlignWinPE(self):
        self.saiFile1 = os.path.join(self.tmpDir, "%s.sai" % self.prefix1)
        self.saiFile2 = os.path.join(self.tmpDir, "%s.sai" % self.prefix2)
        self.bamFile = os.path.join(self.tmpDir, "%s.bam" % self.prefix)
        self.sortFile = os.path.join(self.tmpDir, "%s.sort" % self.prefix)
        self.sortBamFile = os.path.join(self.tmpDir, "%s.sort.bam" % self.prefix)
        self.rmBamFile = os.path.join(self.tmpDir, "%s.sort.rm.bam" % self.prefix)

        ###### align sequence with bwa aln
        self._log("Begin to run bwa aln read 1...")
        self.runSysCommand([self.bwa, 'aln', self.alnPara, self.ref, self.fq1, '>%s' % self.saiFile1], showInLog=True)

        self._log("Begin to run bwa aln read 2...")
        self.runSysCommand([self.bwa, 'aln', self.alnPara, self.ref, self.fq2, '>%s' % self.saiFile2], showInLog=True)

        ##### convert result to sam format
        self._log("Begin to run bwa sampe...")
        self.runSysCommand([self.bwa, 'sampe', '-f', self.samFile, self.ref, self.saiFile1, self.saiFile2, 
                            self.fq1, self.fq2], showInLog=True)

        ##### to get duplication, use samtools
        self._log("Begin to run samtools view...")
        self.runSysCommand([self.samtools, 'view', '-bS', self.samFile, '>', self.bamFile])

        self._log("Begin to run samtools sort...")
        self.runSysCommand([self.samtools, 'sort', self.bamFile, self.sortFile])

        self._log("Begin to run samtools rmdup...")
        dupStr = self.runSysCommandSubProcess([self.samtools, 'rmdup', self.sortBamFile, self.rmBamFile])

        ### get duplication
        dupStr = dupStr.strip().split()
        try:
            self.dupRate = [dupStr[-9], dupStr[-7], dupStr[-5]]
        except Exception as e:
            #raise e 
            self.dupRate = '-'
        return 0

    def setFastqPE(self, fq1, fq2):
        self.readType = 'PE'
        self.fq1 = fq1
        self.fq2 = fq2
        self.prefix1 = os.path.basename(fq1).split('.')[0]
        self.prefix2 = os.path.basename(fq2).split('.')[0]

    def getDupRate(self):
        return self.dupRate

    def buildIndex(self, force=False):
        ##### if not exist bwa index suffix file, rebuild it
        testSuffix = ['.amb', '.ann', '.bwt', '.pac', '.sa']
        existIndex = True
        for f in testSuffix:
            if not os.path.exists(self.ref + f):
                existIndex = False
                break
        if not existIndex or force == True:
            self._log("Not exist bwa index. Rebuilding...")
            subprocess.check_output([self.bwa, 'index', self.ref])
            return

    def updateRef(self):
        self.ref = os.path.join(self._getOptPath('indexDir'), self.reference)
        return self.ref

    def readConfig(self):
        ##### Read option from config file, the default section is 'mapping'
        self.cfg = ConfigParser()
        self.cfg.read(self.configFile)
        self.ref = os.path.join(self._getOptPath('indexDir'), self.cfg.get(self.section, 'reference'))
        # self.tmpDir = self._getOptPath('tmpDir')
        self.tmpDir = self.outDir
        self.alnPara = self.cfg.get(self.section, 'bwaAlnPara')
        self.readType = self.cfg.get(self.section, 'readType')
        self.saveSam = self.cfg.getboolean(self.section, 'saveSam')

        #### parse software path
        self.softVer = self.cfg.get(self.section, 'softVer')
        self.bwa = self._getOptPath('bwa' + '_' + self.softVer)
        self.fqstater = self._getOptPath('fqstater' + '_' + self.softVer)
        self.samtools = self._getOptPath('samtools' + '_' + self.softVer)

        #### init dir
        if self.saveSam == True:
            self.samFile = os.path.join(self.outDir, "%s.sam" % self.prefix)
        else:
            self.samFile = os.path.join(self.tmpDir, "%s.sam" % self.prefix)

        #self.report = os.path.join(self.outDir, '%s_AnalysisReport.html' % self.prefix)

    def runMapping(self):
        #### Check whether exist index
        self.buildIndex()

        #### choose mapping type
        if self.readType == 'SE' and self.platform.startswith('win'):
            self._runAlignWinSE()
        elif self.readType == 'PE' and self.platform.startswith('win'):
            self._runAlignWinPE()
        return 0

    def getSamFile(self):
        return self.samFile

##########################################################################
############################  BEGIN Function  ############################
##########################################################################


######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)
    ArgParser.add_argument("-r", "--ref", action="store", dest="reference", default="Ecoli.fa", metavar="FILE", help="Use specific reference. [%(default)s]")

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 3:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (fqFile, prefix, outPutDir, ) = args

    ############################# Main Body #############################
    fq2sam = Fq2Sam(fqFile, prefix, outPutDir, para.reference, verbose=True)
    fq2sam.updateRef()
    fq2sam.runMapping()
    fq2sam._removeTmpFile()


#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
