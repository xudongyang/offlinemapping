#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
import shutil
### Py2.7 encode fix
reload(sys)
sys.setdefaultencoding('utf8')

import glob
import datetime
from multiprocessing import Pool, cpu_count
import json

from heatmapReport import callCollection, BestFovReport
from fq2sam import Fq2Sam
from splitSamFile import splitSam
from summaryReport import SummaryReport
from mergeSingleCycleHeatmap import mergeSingleCycleHeatmap

###### Document Decription
''' Offline Mapping software for windows platform.
    From zebra's data of fq and qc to final html report.
    Notice, This script is only run in win.
'''

###### Change Log
'''
    1.1.0   11.11   Change name of BioFile.csv to SequencingInfo.csv, and change program's parameters

    1.2.0   2015.11.19  1.Copy FileName.txt BarcodeStat.txt to Fq's dir for generate table's items of chipProd and splitRate
                        2.Add option that control where to find fq and etc, so is data id copied to other dirs or machines,
                            it also works.

    1.3.0   2015.12.4   Output QC csv for QCRun
    1.3.1   2015.12.7   Change QC crietia values

    1.4.0   2016.02.17  Merge all cycle's heatmap to one and it will uploaded to cluster with fqs
    1.5.0   2016.08.18  set threshold for the length of reportname and change GRR to ESR
    
'''

###### Version and Date
prog_version = '1.4.0'
prog_date = '2015-11-11'

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <fovAddr> <prefix> [--ref Ecoli.fa] 
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))
  
###### Global Variable
EMPTY_FQ_SIZE = 100


###### Class Define
class AnalysisMapping(object):
    '''
        Use basecall data as input, and run mapping, then generate html report including 
        1. slide_lane.summaryReport.html 
        2. slide_lane.heatmapReport.html
        3. slide_lane.bestFovReport.html
    '''
    def __init__(self, intensityDir, prefix, bioFile=None, reference='Ecoli.fa', PE=False, mapping=False):
        self.intensityDir = intensityDir
        if len(prefix) > 170:
            self.prefix = prefix[:170]
        else:
            self.prefix = prefix
        self.bioFile = bioFile
        self.reference = reference
        self.PE = PE
        self.mapping = mapping

    def mergeHeatmap(self):
        outHtmlReport = os.path.join(os.path.dirname(self.intensityDir), '%s.allCycleHeatmap.html' % self.prefix)
        mergeSingleCycleHeatmap(self.intensityDir, outHtmlReport)

    def prepareForMap(self, searchDir=False):
        if searchDir:
            infoDict = self.constructInfoDict()
        else:
            infoDict = self.getInfoFromFile()

        if infoDict:
            if len(infoDict['fq']) <= 0:
                print "There is no fq."
                return False
            if infoDict['fq'][0].endswith('.gz'):
                self.fqList = infoDict['fq']
            else:
                self.fqList = [name+'.gz' for name in infoDict['fq']]

            self.resultDir = os.path.dirname(self.fqList[0])
            if 'QC' in infoDict:
                qcFileList = infoDict['QC']
                # Cat all fov's qc file into one BasecallQC.txt
                self.prepareQCData(qcFileList)
            
            # If mapping, then construct directory for mapping
            if self.mapping:
                self.mapDir = os.path.join(self.intensityDir, 'FQMAP')
                if os.path.exists(self.mapDir):
                    try:
                        shutil.rmtree(self.mapDir)
                    except Exception as e:
                        pass
                os.popen(r'mkdir %s' % (self.mapDir, ))
            else:
                self.mapDir = None

            # Change name of bioFile, and copy it to fq's directory
            if self.bioFile is not None and os.path.exists(self.bioFile):
                newBioFile = os.path.join(self.resultDir, 'SequencingInfo.csv')
                shutil.copy(self.bioFile, newBioFile)
                self.bioFile = newBioFile

            # Copy FileName.txt and BarcodeStat.txt to resultDir
            filename = os.path.join(self.intensityDir, 'FileName.txt')
            if os.path.exists(filename):
                shutil.copy(filename, self.resultDir)

            try:
                barcodeStatFile = infoDict['BarcodeStat'][0]
                if os.path.exists(barcodeStatFile):
                    shutil.copy(barcodeStatFile, self.resultDir)
            except Exception, e:
                pass
            return True
        else:
            print "Can't find %s in directory of %s." % ('FileName.txt', self.intensityDir)
            return False

    def runMapping(self):
        # Ensure the cup number is suitable
        if self.reference == 'Human.fa':
            cpuNum = int(int(cpu_count()) / 4)
        else:
            cpuNum = int(cpu_count())

        p = Pool(cpuNum)
        if self.PE:
            for fqFile1, fqFile2 in [[self.fqList[i], self.fqList[i+1]] for i in xrange(0, len(self.fqList), 2)]:
                if not os.path.exists(fqFile1) or not os.path.exists(fqFile2):
                    continue
                if os.path.getsize(fqFile1) <= EMPTY_FQ_SIZE:
                    continue
                p.apply_async(mapOneFqPE, args=(fqFile1, fqFile2, self.mapDir, self.resultDir, self.reference))
        else:
            for fqFile in self.fqList:
                if not os.path.exists(fqFile):
                    continue
                if os.path.getsize(fqFile) <= EMPTY_FQ_SIZE:
                    continue
                p.apply_async(mapOneFqSE, args=(fqFile, self.mapDir, self.resultDir, self.reference))

        p.close()
        p.join()

    def generateHtmlReport(self, csv=False):
        # Mapping or not
        if self.mapping:
            self.runMapping()

        # Generate all report
        outHtml = os.path.join(self.resultDir, '%s.summaryReport.html' % (self.prefix))
        report = SummaryReport(self.prefix, outHtml, None, None, None, None, self.mapDir, None, self.bioFile, self.reference, self.PE, self.resultDir)
        report.prepareData()
        report.generateReport()

        if csv:
            reportData = report.getReportData()
            outCsvFile = os.path.join(self.resultDir, 'QC.csv')
            self.generateQCCsv(reportData, outCsvFile)


        self.geneHeatmapReport()

    def generateQCCsv(self, reportData, outCsvFile):
        if not reportData:
            print 'There is no report data for generating QC.csv'
            return
        # 1. Get Metrics to judge the QC run
        QCCriteriaFile = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'QCCriteria.json')
        if os.path.exists(QCCriteriaFile):
            with open(QCCriteriaFile, 'r') as fhIn:
                data = fhIn.read()
                jsonData = json.loads(data)
        else:
            jsonData = {
                    "Q1": {
                        "TotalReads(M)": ">300", 
                        "ESR%": ">70", 
                        "Q30%": ">70", 
                        "MappingRate%": ">90", 
                        "AvgErrorRate%": "<0.3", 
                        "MaxOffsetX": "<50", 
                        "MaxOffsetY": "<80", 
                        "Lag%": "<0.5", 
                        "Runon%": "<0.25", 
                        "SplitRate%": ">85"
                        }, 
                    "Q2": {
                        "BASE": "<3", 
                        "SNR": ">5.5", 
                        "BIC": ">85", 
                        "FIT": ">70", 
                        "BARCODE": ">70"
                        }
                    }
        
        # 2. Compare each items with Metrics
        options = ['TotalReads(M)', 'ESR%', 'Q30%', 'MappingRate%', 'AvgErrorRate%', 'MaxOffsetX', 'MaxOffsetY',
                    'Lag%', 'Runon%', 'SplitRate%']
        outString = ''
        outString += 'Slide_Lane,%s\n' % self.prefix
        outString += 'Time,%s\n' % reportData['reportTime'].replace('-', '_')
        outString += '\n'
        outString += 'Category,Criteria,Value,Result\n'
        values = []
        if 'summaryTable' in reportData:
            table = reportData['summaryTable']
            tableDict = {item[0]:item[1] for item in table}
            Q1 = jsonData['Q1']
            judge = lambda x: 'Pass' if x else 'Fail'
            for q in options:
                if q not in Q1:
                    continue
                if q in tableDict:
                    value = judge(eval("%s%s" % (tableDict[q], Q1[q])))
                    values.append(value)
                    outString += ','.join([q, str(Q1[q]), str(tableDict[q]), value]) + '\n'
            outString += 'Final,,,%s\n' % ('Fail' if 'Fail' in values else 'Pass')
        
        # 3. Dump to csv file
        with open(os.path.join(self.resultDir, self.prefix+'_QC.csv'), 'w') as fhOut:
            fhOut.write(outString)


    def getInfoFromFile(self):
        filename = os.path.join(self.intensityDir, 'FileName.txt')

        if not os.path.exists(filename):
            return None

        infoDict = {}
        with open(filename, 'r') as fhIn:
            for line in fhIn:
                if line[0] == '#':
                    flag = line.lstrip('#').rstrip()
                    continue
                infoDict.setdefault(flag, []).append(line.strip())

        return infoDict
    
    def constructInfoDict(self):
        infoDict = {}
        resultDir = os.path.dirname(self.intensityDir)
        infoDict['fq'] = sorted(glob.glob(os.path.join(resultDir, '*.fq.gz')))

        finIntPath = os.path.join(self.intensityDir, 'finInts')
        try:
            lastCyclePath = sorted(glob.glob(os.path.join(finIntPath, 'S*')))[-1]
            infoDict['QC'] = sorted(glob.glob(os.path.join(lastCyclePath, 'C*R*.QC.txt')))
        except IndexError, e:
            pass

        
        barcodeStatFile = os.path.join(self.intensityDir, 'BarcodeStat.txt')
        if os.path.exists(barcodeStatFile):
            infoDict['BarcodeStat'] = [barcodeStatFile]

        return infoDict

    def prepareQCData(self, qcFileList):
        qcString = ''

        for qcFile in qcFileList:
            if os.path.exists(qcFile):
                name = os.path.basename(qcFile).split('.')[0]
                with open(qcFile, 'r') as fhIn:
                    qcString += '#FOV\n%s\n' % (name)
                    qcString += fhIn.read()

        if qcString:
            fhOut = open(os.path.join(self.resultDir, 'BasecallQC.txt'), 'w')
            fhOut.write(qcString)
            fhOut.close()

    def geneHeatmapReport(self):
        # Search map file
        if self.mapping:
            splitList = glob.glob(os.path.join(self.mapDir, '*.split.mapInfo.tsv'))
            totalMapFile = None if len(splitList) != 1 else splitList[0]
        else:
            totalMapFile = None

        # search qc file
        totalQcFile = os.path.join(self.resultDir, 'BasecallQC.txt')
        totalQcFile = None if not os.path.exists(totalQcFile) else totalQcFile

        # generate heatmap report
        outHeatmapReport = os.path.join(self.resultDir, '%s.heatmapReport.html' % self.prefix)
        # add exception handle
        try:
            cc = callCollection(totalMapFile=totalMapFile, totalQcFile=totalQcFile)
        except Exception:
            return -1
        if not cc.getFovStat():
            return -1
        #cc = callCollection(totalMapFile=totalMapFile, totalQcFile=totalQcFile)
        #cc.getFovStat()
        cc.getTotalFov(self.resultDir)
        cc.createReport(outHeatmapReport, None)
        bestFov = cc.getBestFov()

        # generate best fov report
        outBestFovReport = os.path.join(self.resultDir, '%s.bestFovReport.html' % self.prefix)
        cc = callCollection(totalMapFile=totalMapFile, totalQcFile=totalQcFile)
        bfd = BestFovReport(bestFov, totalMapFile=totalMapFile, totalQcFile=totalQcFile, pe=self.PE)
        bfd.generateReport(outBestFovReport, self.reference, None, self.bioFile)


def mapOneFqSE(fqFile, mapDir, resultDir, reference):
    # 1. Run mapping
    fqName = os.path.basename(fqFile)
    fqName = fqName.split('.')[0]

    fqStatFile = fqFile[:-3] + '.fqStat.txt' 
    
    fq2sam = Fq2Sam(fqFile, fqName, mapDir, reference, verbose=True)
    fq2sam.updateRef()
    fq2sam.runMapping()
    
    dupRate = fq2sam.getDupRate()
    # Get duplication rate and write in file
    dupFile = os.path.join(mapDir, fqName+'.sort.rmdup.txt')
    with open(dupFile, 'w') as fhOut:
        fhOut.write('\t'.join(dupRate))

    samFile = fq2sam.getSamFile()
    sam = splitSam(samFile, mapDir)
    try:
        sam.readSamFile()
    except IndexError as e:
        fq2sam._removeTmpFile()
        raise e

    totalMapFile = os.path.join(mapDir, fqName+'.mapInfo.tsv')
    splitFov = os.path.join(mapDir, fqName+'.split.mapInfo.tsv')
    sam.statAllSE(totalMapFile, splitFov)

    fq2sam._removeTmpFile()

    outHtml = os.path.join(resultDir, fqName+'.report.html')
    report = SummaryReport(fqName, outHtml, None, None, fqStatFile, None, totalMapFile, dupFile, None, reference, None, None)
    report.generateReport()

def mapOneFqPE(fqFile1, fqFile2, mapDir, resultDir, reference):
    # 1. Run mapping
    fqName = os.path.basename(fqFile1)
    fqName = fqName.split('_')
    fqName = '_'.join(fqName[:-1])

    fqStatFile1 = fqFile1[:-3] + '.fqStat.txt' 
    fqStatFile2 = fqFile2[:-3] + '.fqStat.txt' 
    
    fq2sam = Fq2Sam(fqFile1, fqName, mapDir, reference, verbose=True)
    fq2sam.setFastqPE(fqFile1, fqFile2)
    fq2sam.updateRef()
    fq2sam.runMapping()
    
    dupRate = fq2sam.getDupRate()
    # Get duplication rate and write in file
    dupFile = os.path.join(mapDir, fqName+'.sort.rmdup.txt')
    with open(dupFile, 'w') as fhOut:
        fhOut.write('\t'.join(dupRate))

    samFile = fq2sam.getSamFile()
    sam = splitSam(samFile, mapDir)
    try:
        sam.readSamFile()
    except IndexError as e:
        fq2sam._removeTmpFile()
        raise e

    totalMapFile = os.path.join(mapDir, fqName+'.mapInfo.tsv')
    splitFov = os.path.join(mapDir, fqName+'.split.mapInfo.tsv')
    sam.statAllPE(totalMapFile, splitFov)

    fq2sam._removeTmpFile()

    outHtml = os.path.join(resultDir, fqName+'.report.html')
    report = SummaryReport(fqName, outHtml, None, None, fqStatFile1, fqStatFile2, totalMapFile, dupFile, None, reference, True, None)
    report.generateReport()

#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)
    ArgParser.add_argument("-r", "--ref", action="store", dest="reference", default="Ecoli.fa", metavar="FILE", help="Use specific reference. [%(default)s]")
    ArgParser.add_argument("-b", "--bioFile", action="store", dest="bioFile", default=None, help="Input biochemistry information file. [%(default)s]")
    ArgParser.add_argument("-p", "--PE", action="store_true", dest="PE", default=False, help="Process PE data. [%(default)s]")
    ArgParser.add_argument("-m", "--mapping", action="store_true", dest="mapping", default=False, help="Mapping option. [%(default)s]")
    ArgParser.add_argument("-s", "--searchDir", action="store_true", dest="searchDir", default=False, help="If data was copied to other dir, It will search dir for search fq and etc. [%(default)s]")
    ArgParser.add_argument("-c", "--csv", action="store_true", dest="csv", default=False, help="Generate QC csv option. [%(default)s]")

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 2:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    
    (fovAddr, prefix) = args
    ############################# Main Body #############################
    startTime = datetime.datetime.now()
    print 'Start Time: %s\n' % startTime.strftime("%Y-%m-%d %H:%M:%S")

    mapping = AnalysisMapping(fovAddr, prefix, para.bioFile, para.reference, para.PE, para.mapping)
    # Add merge all cycle heatmap
    mapping.mergeHeatmap()
    if mapping.prepareForMap(para.searchDir):
        mapping.generateHtmlReport(para.csv)

    endTime = datetime.datetime.now()
    print 'End Time: %s\n' % endTime.strftime("%Y-%m-%d %H:%M:%S")
    print 'Generate report spend time %s\n' % (endTime - startTime)

  
#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()
