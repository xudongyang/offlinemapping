#!/usr/bin/bash

cur_date=$(date "+%Y%m%d%H%M")

###### Create dirs to store results
raw_dir=$cur_date
nor_dir=$cur_date'/onlineMapping'
map_dir=$cur_date'/offlineMapping'

for d in $raw_dir $nor_dir $map_dir; do
    if [ ! -e $d ]; then
        mkdir -p $d
    fi
done

###### Process map_dir for fqc offlineMapping
file_list=('analysisAndMapping.py' 'cat.py' 'common.ini' 'fq2sam.py' 
            'fqStatReport.py' 'heatmapReport.py' 'offlineMapping_InstallManual.docx'
            'QCCriteria.json' 'qcReport.py' 'README.txt' 'reference' 'runTest.bat' 
            'samStatReport.py' 'software' 'splitSamFile.py' 'summaryReport.py' 
            'test' 'utility.py' 'version.txt' 'python-2.7.9.amd64.msi' 'mergeSingleCycleHeatmap.py'
            'geneSingleCycleHeatmap.py')
for f in ${file_list[*]}; do
    cp -R $f $map_dir
done

mkdir -p ${map_dir}'/templates'
for f in 'heatmapReportTmpl.html' 'singleCycleHeatmap.html'; do
    cp 'templates/'$f ${map_dir}'/templates'
done
cp 'templates/fqc_template.html' ${map_dir}'/templates/summaryReportTmpl.html'

tar -cf ${map_dir}'.tar' -C $raw_dir 'offlineMapping'

###### Process nor_dir for normal onlineMapping
file_list=('analysisAndMapping.py' 'cat.py' 'samStatReport.py'
            'fqStatReport.py' 'heatmapReport.py' 'fq2sam.py'
            'qcReport.py' 'splitSamFile.py'
            'summaryReport.py' 
            'utility.py' 'version.txt' 'mergeSingleCycleHeatmap.py'
            'geneSingleCycleHeatmap.py' 'geneRealTimeReport.py')
for f in ${file_list[*]}; do
    cp -R $f $nor_dir
done

mkdir -p ${nor_dir}'/templates'
for f in 'heatmapReportTmpl.html' 'singleCycleHeatmap.html'; do
    cp 'templates/'$f ${nor_dir}'/templates'
done
cp 'templates/template.html' ${nor_dir}'/templates/summaryReportTmpl.html'

tar -cf ${nor_dir}'.tar' -C $raw_dir 'onlineMapping'
