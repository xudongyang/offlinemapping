#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
### Py2.7 encode fix
reload(sys)
sys.setdefaultencoding('utf8')
import copy
from copy import deepcopy
from string import maketrans
import subprocess

from cat import CatMapStat
###### Document Decription
'''  '''

###### Version and Date
prog_version = '1.0.2'
prog_date = '2015/02/27'

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s
g
     Usage: %s <samFile> <outPutDir> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class splitSam(object):
    """docstring for samStat"""
    
    def __init__(self, samFile, outPutDir=None):
        # super(samStat, self).__init__()
        if outPutDir == None:
            self.outdir = os.path.dirname(samFile)
        else:
            self.outdir = outPutDir
        self.samFile = samFile

        self.transTable = maketrans('ACGT', 'TGCA')
        self.readLen = 0

        
        self.mis = []
        self.gap = []

        self.preFov = ''
        self.mapInfo = {}

        self.bases = ['A', 'C', 'G', 'T', 'N']
        self.transThreshold = 33

        self.idx = [0, 0]
        self.dupRate = 0


    def initFqStat(self):
        self.baseNum = []
        self.qualityNum = []
        baseDict = {}
        qualityDict = {}

        for base in self.bases:
            baseDict[base] = 0
        for quality in xrange(41):
            qualityDict[quality] = 0

        for i in xrange(self.readLen):
            self.baseNum.append(deepcopy(baseDict))
            self.qualityNum.append(deepcopy(qualityDict))

    def initSamStat(self):

        ### init for next fov
        self.stat = { 'TotalReads': 0,
          'MappedReads': 0,
          'UniqMappedReads': 0,
          'EffectiveReads': 0,
          'ConcordantReads': 0,
          'ReadsWithGap': 0,
          'ReadsWithMismatch': 0,
          'ReadMappedReverse': 0,
          'AvgErrorRate': 0,
        }

        self.misDist = deepcopy(self.mis)
        self.gapDist = deepcopy(self.gap)
       
        self.misNum = {}
        self.gapNum = {}
        self.misSet = set()

    def initFqStatPE(self):
        baseNum = []
        qualityNum = []
        baseDict = {}
        qualityDict = {}

        for base in self.bases:
            baseDict[base] = 0
        for quality in xrange(41):
            qualityDict[quality] = 0

        for i in xrange(self.readLen):
            baseNum.append(deepcopy(baseDict))
            qualityNum.append(deepcopy(qualityDict))

        tmpDict = {'baseNum': baseNum, 'qualityNum': qualityNum}
        self.peFqStat = [deepcopy(tmpDict), deepcopy(tmpDict), deepcopy(tmpDict)]

    def initSamStatPE(self):

        ### init for next fov
        stat = { 'TotalReads': 0,
          'MappedReads': 0,
          'UniqMappedReads': 0,
          'EffectiveReads': 0,
          'ConcordantReads': 0,
          'ReadsWithGap': 0,
          'ReadsWithMismatch': 0,
          'ReadMappedReverse': 0,
          'AvgErrorRate': 0,
        }
        

        misDist1 = deepcopy(self.mis1)
        gapDist1 = deepcopy(self.gap1)

        misDist2 = deepcopy(self.mis2)
        gapDist2 = deepcopy(self.gap2)
       
        misNum = {}
        gapNum = {}
        misSet = set()
        
        tmpDict1 = {'stat': stat, 'misDist': misDist1, 'gapDist': gapDist1, 'misNum': misNum,
                'gapNum': gapNum, 'misSet': misSet}
        tmpDict2 = {'stat': stat, 'misDist': misDist2, 'gapDist': gapDist2, 'misNum': misNum,
                'gapNum': gapNum, 'misSet': misSet}
        tmpDict3 = tmpDict1 if self.readLen1 >= self.readLen2 else tmpDict2

        self.peSamStat = [deepcopy(tmpDict1), deepcopy(tmpDict2), deepcopy(tmpDict3)]

        ### Add pe insert size stat
        self.insertSize = {}

    def _findcol(self, infoCol, marker):
        for idx, c in enumerate(infoCol[11:]):
            if c.startswith(marker):
                return idx + 11
        raise RuntimeError, "Can't find %s column in sam file." % marker

    def readSamFile(self):
        self.inputFile = self.samFile

        if self.inputFile.endswith('.bam'):
            fhIn = os.popen('samtools view %s' % (self.inputFile))
            self.uniqCol = 11
            self.mdzCol = 20
            self.fh = fhIn
            return fhIn

        ########### Open and read input file
        try:
            fhIn = open(self.inputFile, 'rb', 100000000)
        except Exception as e:
            raise e

        firstPos = 0
        firstLine = fhIn.readline()
        ###### Skip Read Group Line
        while firstLine.startswith('@'):
            firstPos = fhIn.tell()
            firstLine = fhIn.readline()

        info = firstLine.split()
        try:
            while int(info[1]) & 4:
                info = fhIn.readline().split()
        except IndexError, e:
            fhIn.close()

            raise IndexError, "No mapped reads were found.File %s\n" % (self.inputFile,)

        ###### lookup uniq mapping marker and MD:Z line
        self.uniqCol = self._findcol(info, 'XT:A')
        self.mdzCol = self._findcol(info, 'MD:Z')

        ###### init mismatch and gap distribution
        self.mis = [ {} for x in range(len(info[9]))]
        self.gap = [ 0 for x in range(len(info[9]))]
        # self.readLen = len(info[9])

        self.misDist = self.mis
        self.gapDist = self.gap
        ###### back to the first line

        fhIn.seek(firstPos)

        ### Judge the read length of two reads
        line = fhIn.readline()
        self.readLen1 = len(line.split()[9])
        self.mis1 = [ {} for x in range(self.readLen1)]
        self.gap1 = [ 0 for x in range(self.readLen1)]

        line = fhIn.readline()
        self.readLen2 = len(line.split()[9])
        self.mis2 = [ {} for x in range(self.readLen2)]
        self.gap2 = [ 0 for x in range(self.readLen2)]

        self.readLen = self.readLen1 if self.readLen1 >= self.readLen2 else self.readLen2
        self.mis = self.mis1 if self.readLen1 >= self.readLen2 else self.mis2
        self.gap = self.gap1 if self.readLen1 >= self.readLen2 else self.gap2

        fhIn.seek(firstPos)

        # print self.readLen, self.readLen1, self.readLen2
        self.fh = fhIn
        return fhIn

    def statAllSE(self, totalMapFile, splitFov=False):
        self.initSamStat()
        splitFovData = []

        lineno = 0
        for line in self.fh:
            info = line.split()
            lineno += 1

            ### Parse the seq ID to process.
            if splitFov:
                seqID = info[0]
                seqID = seqID.split('_')
                fov = seqID[-2][-8:]
            else:
                fov = 'fov'

            if fov not in self.mapInfo:
                self.mapInfo[fov] = {}
                if lineno != 1:
                    splitFovData.append(self.outputSamStat(self.preFov))
                    # init the stat for next fovs.
                    self.initSamStat()

                self.preFov = fov

            ### samStat
            isReverse = int(info[1]) & 16
            isUnmap = int(info[1]) & 4
            seqLen = len(info[9])

            if seqLen > self.readLen:
                self.readLen = seqLen
                self.mis = [ {} for x in range(len(info[9]))]
                self.gap = [ 0 for x in range(len(info[9]))]
                self.initSamStat()

            self.stat['TotalReads'] += 1
            if isUnmap:
                continue

            isUniq = (info[self.uniqCol] == 'XT:A:U')
            if int(info[1]) & 16:
                varList = self.reverseVar(self.findVar(info), seqLen)
            else:
                varList = self.findVar(info)

            misCount = 0
            gapCount = 0

            self.stat['MappedReads'] += 1
            if isReverse:
                self.stat['ReadMappedReverse'] += 1
            if isUniq:
                self.stat['UniqMappedReads'] += 1
            if isUniq and not varList:
                self.stat['EffectiveReads'] += 1
            ##### cal distribution
            for v in varList:
                if v[0] == 0:
                    misCount += 1
                    mismatchType = '%s->%s' % (v[2], v[3])
                    self.misSet.add(mismatchType)
                    if mismatchType not in self.misDist[v[1]-1]:
                        self.misDist[v[1]-1][mismatchType] = 0
                    self.misDist[v[1]-1][mismatchType] += 1 ## TODO, only allow read with same length currently
                else:
                    gapCount += 1
                    self.gapDist[v[1]-1] += 1
            if gapCount:
                self.stat['ReadsWithGap'] += 1
            if misCount:
                self.stat['ReadsWithMismatch'] += 1
            else:
                self.stat['ConcordantReads'] += 1

            ##### cal number type
            if misCount not in self.misNum:
                self.misNum[misCount] = 0
            self.misNum[misCount] += 1
            if gapCount not in self.gapNum:
                self.gapNum[gapCount] = 0
            self.gapNum[gapCount] += 1

        splitFovData.append(self.outputSamStat(self.preFov))

        ### Dump the total map file and split fov's map info if needed
        cat = CatMapStat(is_pe=False, outfile=totalMapFile)
        cat.cat(splitFovData)
        if splitFov:
            fhOut = open(splitFov, 'w')
            fhOut.write(''.join(splitFovData))
            fhOut.close()

        self.fh.close()

    def statAllPE(self, totalMapFile, splitFov=False):
        splitFovData = []
        if self.inputFile.endswith('.sam'):
            self.initSamStatPE()

        line1 = 1
        lineNum = 0
        while line1:

            line1 = self.fh.readline()
            line2 = self.fh.readline()

            if not line1:
                break
    
            info = line1.split()
            if splitFov:
                seqID = info[0]
                seqID = seqID.split('_')
                fov = seqID[-2][-8:]
            else:
                fov = 'fov'
           
            if fov not in self.mapInfo:
               self.mapInfo[fov] = {}
               if lineNum != 0:
                    splitFovData.append(self.outputSamStatPE(self.preFov))
                    self.initSamStatPE()

               self.preFov = fov

            # init
            seqLen1 = len(line1.split()[9])
            seqLen2 = len(line2.split()[9])
            if seqLen1 > self.readLen and seqLen2 > self.readLen:
                self.readLen1 = seqLen1
                self.mis1 = [ {} for x in range(self.readLen1)]
                self.gap1 = [ 0 for x in range(self.readLen1)]
                self.readLen2 = seqLen2
                self.mis2 = [ {} for x in range(self.readLen2)]
                self.gap2 = [ 0 for x in range(self.readLen2)]
                self.readLen = self.readLen1 if self.readLen1 >= self.readLen2 else self.readLen2

                self.initSamStatPE()

            ### Add pe insert size stat
            info2 = line2.split()
            if info[2] == info2[2]:
                subValue = int(info[3]) - int(info2[3])
                if subValue > 0:
                    subValue += self.readLen1
                    if subValue not in self.insertSize:
                        self.insertSize[subValue] = 0
                    self.insertSize[subValue] += 1
                elif subValue < 0:
                    subValue = self.readLen2 - subValue
                    if subValue not in self.insertSize:
                        self.insertSize[subValue] = 0
                    self.insertSize[subValue] += 1
               
            lineNum += 2
            ### Travels the two sequence.
            allMappend = False
            tableList = []
            for idx in xrange(2):
                line = line1 if idx == 0 else line2
                info = line.split()

                ### 1. Now, no Fq stat.
                isReverse = int(info[1]) & 16

                ### 2. Mapping stat
                tmpStatDict = {}
                isUnmap = int(info[1]) & 4
                seqLen = len(info[9])
                isMateUnmap = int(info[1]) & 8

                currentDict = self.peSamStat[idx]
                commonDict = self.peSamStat[2]

                currentDict['stat']['TotalReads'] += 1
                if isUnmap:
                    continue


                isUniq = (info[self.uniqCol] == 'XT:A:U')
                varList = self.findVar(info)
                if isinstance(varList, bool):
                    continue

                if int(info[1]) & 16:
                    # print info[0]
                    varList = self.reverseVar(varList, seqLen)

                misCount = 0
                gapCount = 0

                currentDict['stat']['MappedReads'] += 1
                if isReverse:
                    currentDict['stat']['ReadMappedReverse'] += 1
                if isUniq:
                    currentDict['stat']['UniqMappedReads'] += 1
                if isUniq and not varList:
                    currentDict['stat']['EffectiveReads'] += 1
                ##### cal distribution
                for v in varList:
                    if v[0] == 0:
                        misCount += 1
                        mismatchType = '%s->%s' % (v[2], v[3])
                        currentDict['misSet'].add(mismatchType)
                        if mismatchType not in currentDict['misDist'][v[1]-1]:
                            currentDict['misDist'][v[1]-1][mismatchType] = 0
                        currentDict['misDist'][v[1]-1][mismatchType] += 1 ## TODO, only allow read with same length currently
                    else:
                        gapCount += 1
                        currentDict['gapDist'][v[1]-1] += 1
                if gapCount:
                    currentDict['stat']['ReadsWithGap'] += 1
                if misCount:
                    currentDict['stat']['ReadsWithMismatch'] += 1
                else:
                    currentDict['stat']['ConcordantReads'] += 1


                ##### cal number type
                if misCount not in currentDict['misNum']:
                    currentDict['misNum'][misCount] = 0
                currentDict['misNum'][misCount] += 1
                if gapCount not in currentDict['gapNum']:
                    currentDict['gapNum'][gapCount] = 0
                currentDict['gapNum'][gapCount] += 1

                ### Stat
                tmpStatDict['isUniq'] = isUniq
                tmpStatDict['isEffective'] = (isUniq and not varList)
                tmpStatDict['isMismatch'] = misCount
                tmpStatDict['isConcordant'] = (not misCount)
                tmpStatDict['isGap'] = gapCount
                tmpStatDict['isReverse'] = isReverse
                
                tableList.append(tmpStatDict)

                ### If all mapped, then add the stat info to commonDict

                misCount = 0
                gapCount = 0
                if not isUnmap and not isMateUnmap:
                    allMappend = True
                    ##### cal distribution
                    for v in varList:
                        if v[0] == 0:
                            misCount += 1
                            mismatchType = '%s->%s' % (v[2], v[3])
                            commonDict['misSet'].add(mismatchType)
                            if mismatchType not in commonDict['misDist'][v[1]-1]:
                                commonDict['misDist'][v[1]-1][mismatchType] = 0
                            commonDict['misDist'][v[1]-1][mismatchType] += 1 ## TODO, only allow read with same length currently
                        else:
                            gapCount += 1
                            commonDict['gapDist'][v[1]-1] += 1

                    ##### cal number type
                    if misCount not in commonDict['misNum']:
                        commonDict['misNum'][misCount] = 0
                    commonDict['misNum'][misCount] += 1
                    if gapCount not in commonDict['gapNum']:
                        commonDict['gapNum'][gapCount] = 0
                    commonDict['gapNum'][gapCount] += 1

            commonDict = self.peSamStat[2]
            commonDict['stat']['TotalReads'] += 1
            if allMappend:
                commonDict['stat']['MappedReads'] += 1
                if len(tableList) != 2:
                    continue
                else:
                    if tableList[0]['isUniq'] and tableList[1]['isUniq']:
                        commonDict['stat']['UniqMappedReads'] += 1  
                    if tableList[0]['isEffective'] and tableList[1]['isEffective']:
                        commonDict['stat']['EffectiveReads'] += 1  
                    if tableList[0]['isMismatch'] or tableList[1]['isMismatch']:
                        commonDict['stat']['ReadsWithMismatch'] += 1  
                    if tableList[0]['isGap'] or tableList[1]['isGap']:
                        commonDict['stat']['ReadsWithGap'] += 1  
                    if (tableList[0]['isReverse'] and tableList[1]['isReverse']) or \
                        ((not tableList[0]['isReverse']) and (not tableList[1]['isReverse'])):
                        commonDict['stat']['ReadMappedReverse'] += 1 
                    if tableList[0]['isConcordant'] and tableList[1]['isConcordant']:
                        commonDict['stat']['ConcordantReads'] += 1 



        splitFovData.append(self.outputSamStatPE(self.preFov))

        ### Dump the total map file and split fov's map info if needed
        cat = CatMapStat(is_pe=True, outfile=totalMapFile)
        cat.cat(splitFovData)
        if splitFov:
            fhOut = open(splitFov, 'w')
            fhOut.write(''.join(splitFovData))
            fhOut.close()

        self.fh.close()

    def calSamStatSingleChain(self, samStatDict, readLen, flag):
        stat = samStatDict['stat']
        misNum = samStatDict['misNum']
        gapNum = samStatDict['gapNum']
        misDist = samStatDict['misDist']
        gapDist = samStatDict['gapDist']
        misSet = samStatDict['misSet']

        resultString = '----------%s\n' % (flag,)

        resultString += '#%s\n' % (os.path.join(self.outdir, self.preFov))
        #fhOut.write("#%s\n" % self.inputFile)
        resultString += "MaxReadsLength\t%d\n" % readLen
        resultString += "TotalReads\t%s\n" % stat['TotalReads']

        for key in ('MappedReads', 'UniqMappedReads', 'EffectiveReads', 'ConcordantReads', 'ReadsWithMismatch', 'ReadsWithGap', 'ReadMappedReverse'):
            tmpList = [key, stat[key], round(self.getPctPE(key, 'TotalReads', stat), 4), round(self.getPctPE(key, 'MappedReads', stat), 4)]
            resultString += "\t".join(map(str, tmpList))
            resultString += '\n'

        ### add the dup Rate
        #resultString += 'DuplicationRate\t%.2f\n' % self.dupRate

        # print misNum

        resultString += "MismatchNumberPerReads\t" + "\t".join(map(str, self.getNumCount(misNum))) + '\n'
        resultString += "GapNumberPerReads\t" + "\t".join(map(str, self.getNumCount(gapNum))) + '\n'
        resultString += "MismatchRate\t%s" % self.getAvgErrPE(stat, misDist) + '\n'
        resultString += "MismatchDistribution\t" + "\t".join(map(str, self.getSumMisDistPE(misDist))) + '\n'
        resultString += "GapDistribution\t" + "\t".join(map(str, gapDist)) + '\n'

        ##### print all mismatch type
        for i in self.getMisTypePE(misSet, misDist):
            resultString += "MisType_%s\t" % i[0] + "\t".join(map(str, i[1:]))
            resultString += '\n'

        return resultString

    def outputSamStatPE(self, fovNum):
        tmpString = '#fov\n%s\n' % fovNum

        tmpString += self.calSamStatSingleChain(self.peSamStat[0], self.readLen1, 'read1')
        tmpString += self.calSamStatSingleChain(self.peSamStat[1], self.readLen2, 'read2')
        # print self.peSamStat[2]
        for key in self.peSamStat[2].keys():
            if key == 'misNum' or key == 'gapNum':
                for k in self.peSamStat[2][key].keys():
                    self.peSamStat[2][key][k] /= 2

        tmpString += self.calSamStatSingleChain(self.peSamStat[2], self.readLen, 'readTotal')

        ### Add pe insert size stat
        if self.insertSize:
            insertString = '#insertSize\n'
            d = self.insertSize
            insertString += '\t'.join(map(str, sorted(d.keys())))
            insertString += '\n'
            insertString += '\t'.join(map(str, [d[k] for k in sorted(d.keys())]))
            tmpString += insertString
    
        return tmpString

    def calFqStatSingleChain(self, fqStatDict):
        BaseNum = fqStatDict['baseNum']
        QualityNum = fqStatDict['qualityNum']

        ### 1. Calcluate the result
        baseNum = 0
        N_count = 0
        GC_count = 0
        for i in xrange(len(BaseNum)):
            for k in BaseNum[i].keys():
                baseNum += BaseNum[i][k]
            N_count += BaseNum[i]['N']
            GC_count += BaseNum[i]['G']
            GC_count += BaseNum[i]['C']

        # print baseNum, self.readLen
        readNum = int(baseNum / self.readLen)
        N_count_per = round((float(N_count)/baseNum)*100, 2)
        GC_per = round(GC_count*100 / float(baseNum - N_count), 2)
        

        Q10_count = 0
        Q20_count = 0
        Q30_count = 0
        #Q40_count = 0
        for i in xrange(len(QualityNum)):
            for j in xrange(10, 41):
                Q10_count += QualityNum[i][j]
            for j in xrange(20, 41):
                Q20_count += QualityNum[i][j]
            for j in xrange(30, 41):
                Q30_count += QualityNum[i][j]
            #for j in xrange(40, 41):
                #Q40_count += QualityNum[i][j]

        Q10_per = round(Q10_count*100 / float(baseNum), 2)
        Q20_per = round(Q20_count*100 / float(baseNum), 2)
        Q30_per = round(Q30_count*100 / float(baseNum), 2)
        #Q40_per = round(Q40_count*100 / float(baseNum), 2)
           
        errList = []
        for c in xrange(self.readLen):
            #errValue = 0
            lst = []
            for i in xrange(41):
                lst.append(1.0/pow(10, i*(0.1)) * (QualityNum[c][i]/float(readNum)))#* ((mat[0,i+6])/readNum)
            s = sum(lst)
            errList.append(round(float(s)*100, 4))
       
        Est_err_per = round(sum(errList) / float(len(errList)), 2)

        ### 2. Transform the result to a string and return
        resultString = '----------\n'
        resultString += '#Name\t%s\n' % (os.path.join(self.outdir, self.preFov))
        resultString += '#PhredQual\t%d\n' % self.transThreshold
        resultString += '#ReadNum\t%d\n' % readNum
        resultString += '#BaseNum\t%d\n' % baseNum
        resultString += '#N_Count\t%d\t%.2f\n' % (N_count, N_count_per)
        resultString += '#GC%%\t%.2f\n' % GC_per
        resultString += '#Q10%%\t%.2f\n' % Q10_per
        resultString += '#Q20%%\t%.2f\n' % Q20_per
        resultString += '#Q30%%\t%.2f\n' % Q30_per
        #resultString += '#>Q40%%\t%.2f\n' % Q40_per
        resultString += '#EstErr%%\t%.2f\n' % Est_err_per
        resultString += '#Pos\tA\tC\tG\tT\tN\t'
        resultString += '\t'.join(map(str,xrange(41)))
        resultString += '\tErr%\n'
        
        qualityStr = ''
        for i in xrange(self.readLen):
            qualityStr += '%d\t' % (i+1)
            for k in self.bases:
                #print type(self.baseNum[i][k])
                qualityStr += '%d\t' % BaseNum[i][k]
            for k in sorted(QualityNum[i].keys()):
                qualityStr += '%d\t' % QualityNum[i][k]
            qualityStr += '%.4f\n' % errList[i]

        resultString += qualityStr

        return resultString

    def outputFqStatPE(self, fileName):
        fhOut = open(fileName, 'w')

        fhOut.write(self.calFqStatSingleChain(self.peFqStat[0]))
        fhOut.write(self.calFqStatSingleChain(self.peFqStat[1]))
        fhOut.write(self.calFqStatSingleChain(self.peFqStat[2]))
        fhOut.close()

    def outputSamStat(self, fovNum):
        tmpString = '#fov\n%s\n' % fovNum

        tmpString += "#%s\n" % self.inputFile
        tmpString += "MaxReadsLength\t%s\n" % self.readLen
        tmpString += "TotalReads\t%s\n" % self.stat['TotalReads']

        for key in ('MappedReads', 'UniqMappedReads', 'EffectiveReads', 'ConcordantReads', 'ReadsWithMismatch', 'ReadsWithGap', 'ReadMappedReverse'):
            tmpList = [key, self.stat[key], round(self.getPct(key, 'TotalReads'), 4), round(self.getPct(key, 'MappedReads'), 4)]
            tmpString += "\t".join(map(str, tmpList)) + '\n'
            
        ### add the dup Rate
        #fhOut.write('DuplicationRate\t%.2f\n' % self.dupRate)

        tmpString += "MismatchNumberPerReads\t" + "\t".join(map(str, self.getNumCount(self.misNum))) + '\n'
        tmpString += "GapNumberPerReads\t" + "\t".join(map(str, self.getNumCount(self.gapNum))) + '\n'
        tmpString += "MismatchRate\t%s" % self.getAvgErr() + '\n'
        tmpString += "MismatchDistribution\t" + "\t".join(map(str, self.getSumMisDist())) + '\n'
        tmpString += "GapDistribution\t" + "\t".join(map(str, self.gapDist)) + '\n'

        ##### print all mismatch type
        for i in self.getMisType():
            tmpString += "MisType_%s\t" % i[0] + "\t".join(map(str, i[1:])) + '\n'

        return tmpString

    def outputFqStat(self, fileName):
        baseNum = 0
        N_count = 0
        GC_count = 0
        for i in xrange(len(self.baseNum)):
            for k in self.baseNum[i].keys():
                baseNum += self.baseNum[i][k]
            N_count += self.baseNum[i]['N']
            GC_count += self.baseNum[i]['G']
            GC_count += self.baseNum[i]['C']

        readNum = int(baseNum / self.readLen)
        N_count_per = round((float(N_count)/baseNum)*100, 2)
        GC_per = round(GC_count*100 / float(baseNum - N_count), 2)
        

        Q10_count = 0
        Q20_count = 0
        Q30_count = 0
        #Q40_count = 0
        for i in xrange(len(self.qualityNum)):
            for j in xrange(10, 41):
                Q10_count += self.qualityNum[i][j]
            for j in xrange(20, 41):
                Q20_count += self.qualityNum[i][j]
            for j in xrange(30, 41):
                Q30_count += self.qualityNum[i][j]
            #for j in xrange(40, 41):
                #Q40_count += self.qualityNum[i][j]

        Q10_per = round(Q10_count*100 / float(baseNum), 2)
        Q20_per = round(Q20_count*100 / float(baseNum), 2)
        Q30_per = round(Q30_count*100 / float(baseNum), 2)
        #Q40_per = round(Q40_count*100 / float(baseNum), 2)
           
        errList = []
        for c in xrange(self.readLen):
            #errValue = 0
            lst = []
            for i in xrange(41):
                lst.append(1.0/pow(10, i*(0.1)) * (self.qualityNum[c][i]/float(readNum)))#* ((mat[0,i+6])/readNum)
            s = sum(lst)
            errList.append(round(float(s)*100, 4))
       
        Est_err_per = round(sum(errList) / float(len(errList)), 2)
               
        ### 3. Output the info of fovReport fqState.
        fhOut = open(fileName, 'w')

        fhOut.write('#Name\t%s\n' % (os.path.join(self.outdir, self.preFov)))
        fhOut.write('#PhredQual\t%d\n' % self.transThreshold)
        fhOut.write('#ReadNum\t%d\n' % readNum)
        fhOut.write('#BaseNum\t%d\n' % baseNum)
        fhOut.write('#N_Count\t%d\t%.2f\n' % (N_count, N_count_per))
        fhOut.write('#GC%%\t%.2f\n' % GC_per)
        fhOut.write('#Q10%%\t%.2f\n' % Q10_per)
        fhOut.write('#Q20%%\t%.2f\n' % Q20_per)
        fhOut.write('#Q30%%\t%.2f\n' % Q30_per)
        #fhOut.write('#>Q40%%\t%.2f\n' % Q40_per)
        fhOut.write('#EstErr%%\t%.2f\n' % Est_err_per)
        fhOut.write('#Pos\tA\tC\tG\tT\tN\t')
        fhOut.write('\t'.join(map(str,xrange(41))))
        fhOut.write('\tErr%\n')
        
        for i in xrange(self.readLen):
            fhOut.write('%d\t' % (i+1))
            for k in self.bases:
                #print type(self.baseNum[i][k])
                fhOut.write('%d\t' % self.baseNum[i][k])
            for k in sorted(self.qualityNum[i].keys()):
                fhOut.write('%d\t' % self.qualityNum[i][k])
            fhOut.write('%.4f\n' % errList[i])

        fhOut.close()

    ### The method for pair-end
    def getPctPE(self, key, fa, stat):
        #print fa, self.stat[fa]
        if stat[fa] != 0:
            return float(stat[key])/stat[fa]
        else:
            return 0.0

    def getSumMisDistPE(self, misDist):
        return [sum(x.values()) for x in misDist]

    def getAvgErrPE(self, stat, misDist):
        try:
            return round(float(sum(self.getSumMisDistPE(misDist)))/self.readLen/stat['MappedReads'], 4)
        except ZeroDivisionError:
            return 0

    def getMisTypePE(self, misSet, misDist, noN=False):
        tmpList = []
        for k in sorted(list(misSet)):
            if noN == True and (k.startswith('N') or k.endswith('N')):
                continue
            tmpList.append([k] + [x.get(k, 0) for x in misDist])
        return tmpList


    ### The method for single-end
    def getPct(self, key, fa):
        #print fa, self.stat[fa]
        if self.stat[fa] != 0:
            return float(self.stat[key])/self.stat[fa]
        else:
            return 0.0

    def getSumMisDist(self):
        return [sum(x.values()) for x in self.misDist]

    def getAvgErr(self):
        try:
            return round(float(sum(self.getSumMisDist()))/self.readLen/self.stat['MappedReads'], 4)
        except ZeroDivisionError:
            return 0

    def getMisType(self, noN=False):
        tmpList = []
        for k in sorted(list(self.misSet)):
            if noN == True and (k.startswith('N') or k.endswith('N')):
                continue
            tmpList.append([k] + [x.get(k, 0) for x in self.misDist])
        return tmpList

    def _pctString(self, key, toStdout=True):
        tmpList = [key, self.stat[key], round(self.getPct(key, 'TotalReads'), 4), round(self.getPct(key, 'MappedReads'), 4)]
        if toStdout == True:
            print "\t".join(map(str, tmpList))
        return tmpList

    
    

    def getNumCount(self, d):
        try:   
            maxCount = max(d.keys())
        except ValueError:
            return '0'

        tmpArr = []
        for x in xrange(0, maxCount+1):
            if x not in d:
                tmpArr.append(0)
            else:
                tmpArr.append(d[x])
        return tmpArr

    
        
    def revCompSeq(self, seq):
        trans = seq.translate(self.transTable)
        return trans[::-1]

    def reverseVar(self, varList, seqLen):
        for v in varList:
            if v[0] == 0: ## type is mismatch
                v[1] = seqLen - v[1] + len(v[2])
            elif v[0] == 1: ## type is insertion
                v[1] = seqLen - (v[1] + len(v[2]))
            else: ## type is deletion
                v[1] = seqLen - v[1]
            v[2] = self.revCompSeq(v[2])
            v[3] = self.revCompSeq(v[3])
        return varList

    def mdzToList(self, mdz):
        ''' Parse MD:Z string into a list of operations, where 0=match,
            1=read gap, 2=mismatch. '''
        md = mdz[5:]
        i = 0;
        ret = [] # list of (op, run, str) tuples
        while i < len(md):
            if md[i].isdigit(): # stretch of matches
                run = 0
                while i < len(md) and md[i].isdigit():
                    run *= 10
                    run += int(md[i])
                    i += 1 # skip over digit
                if run > 0:
                    ret.append([0, run, ""])
            elif md[i].isalpha(): # stretch of mismatches
                mmstr = ""
                while i < len(md) and md[i].isalpha():
                    mmstr += md[i]
                    i += 1
                assert len(mmstr) > 0
                ret.append([1, len(mmstr), mmstr])
            elif md[i] == "^": # read gap
                i += 1 # skip over ^
                refstr = ""
                while i < len(md) and md[i].isalpha():
                    refstr += md[i]
                    i += 1 # skip over inserted character
                assert len(refstr) > 0
                ret.append([2, len(refstr), refstr])
            else:
                raise RuntimeError('Unexpected character in MD:Z: "%d"' % md[i])
        return ret

    def cigarToList(self, cigar):
        ''' Parse CIGAR string into a list of CIGAR operations.  For more
            info on CIGAR operations, see SAM spec:
            http://samtools.sourceforge.net/SAMv1.pdf '''
        ret, i = [], 0
        op_map = {'M':0, # match or mismatch
                  '=':0, # match
                  'X':0, # mismatch
                  'I':1, # insertion in read w/r/t reference
                  'D':2, # deletion in read w/r/t reference
                  'N':3, # long gap due e.g. to splice junction
                  'S':4, # soft clipping due e.g. to local alignment
                  'H':5, # hard clipping
                  'P':6} # padding
        # Seems like = and X together are strictly more expressive than M.
        # Why not just have = and X and get rid of M?  Space efficiency,
        # mainly.  The titans discuss: http://www.biostars.org/p/17043/
        while i < len(cigar):
            run = 0
            while i < len(cigar) and cigar[i].isdigit():
                # parse one more digit of run length
                run *= 10
                run += int(cigar[i])
                i += 1
            assert i < len(cigar)
            # parse cigar operation
            op = cigar[i]
            i += 1
            assert op in op_map
            # append to result
            ret.append([op_map[op], run])
        return ret

    def findVar(self, infoCol):
        ####### Example, CIGAR: 28M1I5M   MD:Z 28M1I5M
        ####### cigarList: [[0, 28], [1, 1], [0, 5]]
        ####### mdzList:   [[0, 32, ''], [1, 1, 'G']]
        cigarList = self.cigarToList(infoCol[5])
        if self.mdzCol >= len(infoCol) or not infoCol[self.mdzCol].startswith('MD:Z'):
            self.mdzCol = self._findcol(infoCol, 'MD:Z')
        mdzList = self.mdzToList(infoCol[self.mdzCol])
        mdIdx = 0 ## the index position in mdzlist
        seqPos = 0
        varList = []
        for cig in cigarList:
            cigType, cigLen = cig
            try:
                assert cigType == 4 or cigType == 5 or mdIdx < len(mdzList)
            except AssertionError:
                return False
            if cigType == 0: ## CIGAR is M, =, X
                cigLeft = cigLen
                while cigLeft > 0 and mdIdx < len(mdzList):
                    mdType, mdLen, mdBase = mdzList[mdIdx]
                    lenComb = min(cigLeft, mdLen)
                    cigLeft -= lenComb
                    assert mdType == 0 or mdType == 1 ## MDZ should be match or mismatch
                    if mdType == 1: ## MD:Z got a mismatch
                        assert len(mdBase) == lenComb
                        for pos in range(mdLen):
                            seqPos += 1
                            #### type, position on seq, ref base, reads base, 0 means mismatch
                            varList.append([0, seqPos, mdBase[pos], infoCol[9][seqPos-1] ])
                            
                    else: ## MD:Z got a match
                        seqPos += lenComb

                    if lenComb < mdLen:
                        assert mdType == 0
                        mdzList[mdIdx][1] -= lenComb
                    else:
                        mdIdx += 1
            elif cigType == 1: ## CIGAR is I
                ####### reads insert against to reference, 1 means insertion
                varList.append([1, seqPos, '-' * cigLen, infoCol[9][seqPos:seqPos+cigLen]])
                seqPos += cigLen ## CIGAR move forward, but not apper on MD:Z
            elif cigType == 2: ## CIGAR is D
                mdType, mdLen, mdBase = mdzList[mdIdx]
                assert mdType == 2 and cigLen == mdLen and cigLen == len(mdBase)
                varList.append([2, seqPos, mdBase, '-' * mdLen])
                # seqPos += mdLen
                mdIdx += 1
            elif cigType in (3, 4, 5): ## CIGAR is N, S, H
                seqPos += cigLen
                ######## TODO: check here whether appropriated for N
            else:
                raise RuntimeError, "Unknown CIGAR string: %s" % cigType
        return varList

    def setDupRate(self, dupRate):
        self.dupRate = dupRate


##########################################################################
############################  BEGIN Function  ############################
##########################################################################


######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 2:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (samFile, outPutDir) = args
    ############################# Main Body #############################
    sam = splitSam(samFile, outPutDir)
    sam.readSamFile()
    #sam.statAllSE()
    sam.statAllSEOnlyOneFq('test')
    

#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
