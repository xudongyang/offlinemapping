import os, sys
import glob
import re
import json
import unittest

from geneSingleCycleHeatmap import callCollection

class ReadHtml(object):
    ''' This class is just for read html report '''
    def __init__(self, html):
        self.html = html
        if not os.path.exists(html): 
            print "There is no html report of %s." % html
            sys.exit(-1)

    def read_html(self):
        rawData = {}
        with open(self.html, 'r') as fh_in:
            in_data = False
            for line in fh_in:
                if re.search('VALUES BEGIN', line):
                    in_data = True
                elif re.search('VALUES END', line):
                    in_data = False
                    break
                elif in_data:
                    line = line.strip()
                    if line and not line.startswith('//'):
                        rawData.update(self._get_oneline(line))
                else:
                    pass
        return rawData

    def _get_oneline(self, line):
        line = line.strip().lstrip('var').rstrip(';')
        line = line.strip().split('=')
        # Transform ' to ", then json can get it
        data = line[1].replace("'", '"')
        jsonData = json.loads(data)
        return {line[0].strip(): jsonData}

def mergeSingleCycleHeatmap(fovAddr, outHtmlReport):
    cyclesDir = glob.glob(os.path.join(fovAddr, 'finInts', "S*"))
    if not cyclesDir: 
        print "There is not exists finInts in %s" % fovAddr
        return 

    cyclesDir.sort()
    fovData = {}
    option = []
    valLimit = {}
    for cycleDir in cyclesDir:
        cycle = int(os.path.basename(cycleDir)[1:])
        html = os.path.join(cycleDir, 'cycle%d_Heatmap.html' % cycle)
        if not os.path.exists(html): continue

        rh = ReadHtml(html)
        rawData = rh.read_html()
        if rawData: 
            if 'fovData' in rawData and rawData['fovData']:
                fovData.update(rawData['fovData'])
            if 'option' in rawData and rawData['option']:
                if not option: 
                    option = rawData['option']
                else: 
                    if option != rawData['option']:
                        print "There is not exists same option in %s" % (html, )
                        break
            if 'valLimit' in rawData and rawData['valLimit']:
                if not valLimit: 
                    valLimit = rawData['valLimit']
                else: 
                    if valLimit != rawData['valLimit']:
                        print "There is not exists same valLimit in %s" % (html, )
                        break
    if not fovData or not option: return
    cc = callCollection(fovData)
    cc.setPara(fovData, option, valLimit)
    cc.createReport(outHtmlReport)

def main():
    if len(sys.argv) != 3:
        print "python %s <fovAddr> <outHtml>" % sys.argv[0]
        sys.exit(-1)
    fovAddr = sys.argv[1]
    outHtml = sys.argv[2]
    mergeSingleCycleHeatmap(fovAddr, outHtml)

if __name__ == "__main__":
    main()


