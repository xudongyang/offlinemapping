#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
### Py2.7 encode fix
reload(sys)
sys.setdefaultencoding('utf8')

import re
import copy
import time
import datetime
import glob
import string

# import pprint
# import cPickle as pickle
from qcReport import analysis
from geneSingleCycleHeatmap import geneOnlyIntenHeatmap, geneAllInfoHeatmap, dumpDnbLoad2Csv
from cat import CatQcStat
from summaryReport import transformReport

###### Document Decription
'''  '''

###### Version and Date
prog_version = '2.0.0'
prog_date = '2015.05.26'

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <fovAddr> <currentCycle> <maxCycle> <windowSize>
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable

#######################################################################
############################  BEGIN Class  ############################
#######################################################################

##########################################################################
############################  BEGIN Function  ############################
##########################################################################
def geneRealTimeReport(fovAddr, cycle, maxCycle, window):
    cycle = int(cycle)
    maxCycle = int(maxCycle)
    window = int(window)
    currentDir = os.path.join(fovAddr, 'S%03d' % cycle)
    filename = os.path.join(currentDir, 'cycle%d_AnalysisReport.html' % cycle)
    # secondDir = os.path.join(fovAddr, 'S%03d' % cycle)

    ### 1.Generate basecall info report.
    qcFiles = glob.glob(os.path.join(currentDir, 'C*R*.QC.txt'))
    basecallInfo = os.path.join(currentDir, 'fovReport.QC.txt')

    cat = CatQcStat(basecallInfo)
    cat.cat(qcFiles)

    prefix = 'cycle' + str(cycle)
    geneQCReport(basecallInfo, prefix, filename)

    ### 2. Generate heatmap info report.
    heatmapReport1 = os.path.join(currentDir, 'cycle%d_Heatmap.html' % cycle)

    
    if cycle < (window*2 + 1):
        statData = geneAllInfoHeatmap(currentDir, cycle, currentDir, cycle, heatmapReport1)
        dumpDnbLoad2Csv(statData, os.path.join(currentDir, 'dnbLoading.csv'))
    elif cycle < maxCycle:
        geneOnlyIntenHeatmap(currentDir, cycle, heatmapReport1)

        secondCycle = cycle-window*2
        secondDir = os.path.join(fovAddr, 'S%03d' % secondCycle)
        
        heatmapReport2 = os.path.join(secondDir, 'cycle%d_Heatmap.html' % secondCycle)
        statData = geneAllInfoHeatmap(secondDir, secondCycle, currentDir, secondCycle, heatmapReport2)
        dumpDnbLoad2Csv(statData, os.path.join(secondDir, 'dnbLoading.csv'))

    else:
        statData = geneAllInfoHeatmap(currentDir, cycle, currentDir, cycle, heatmapReport1)
        dumpDnbLoad2Csv(statData, os.path.join(currentDir, 'dnbLoading.csv'))
        for c in xrange(maxCycle - 2*window, maxCycle):
            secondCycle = c
            secondDir = os.path.join(fovAddr, 'S%03d' % secondCycle)
            
            heatmapReport2 = os.path.join(secondDir, 'cycle%d_Heatmap.html' % secondCycle)
            statData = geneAllInfoHeatmap(secondDir, secondCycle, currentDir, secondCycle, heatmapReport2)
            dumpDnbLoad2Csv(statData, os.path.join(secondDir, 'dnbLoading.csv'))


def geneQCReport(basecallFile, prefix, filename):
    jsonData = {}

    reportTime = time.strftime('%Y-%m-%d', time.localtime(time.time()))
    reportTitle = 'Analysis Report of %s' % (prefix)
    jsonData['reportTitle'] = '"%s"' % reportTitle
    jsonData['reportTime'] = '"%s"' % reportTime

    stat = analysis(basecallFile, doPhase=False)
    jsonData.update(stat.geneQcJsonData())
    # wipe off background figure
    if 'BACKGROUND' in jsonData:
        del jsonData['BACKGROUND']

    transformReport(None, filename, jsonData)


######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 4:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (fovAddr, cycle, maxCycle, window) = args

    ############################# Main Body #############################
    # startTime = datetime.datetime.now()

    geneRealTimeReport(fovAddr, cycle, maxCycle, window)

    # endTime = datetime.datetime.now()

    # print (endTime - startTime)

#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
