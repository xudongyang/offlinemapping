#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
reload(sys)
sys.setdefaultencoding('utf8')
import re
import glob
import json
import string

import math
# import pprint
# import cPickle as pickle

###### Document Decription
'''  '''

###### Version and Date
prog_version = '1.1.3'
prog_date = '2015-04-03'

###### Chang Log
'''
    1.5.0   2016.08.18  recover the BACKGROUND item add lag runon item, move limit to template
'''

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <inputDir> <report> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable

#######################################################################
############################  BEGIN Class  ############################
#######################################################################

       
class prepraeData(object):

    def __init__(self, statData):
        self.statData = statData
        self.intenOption = ['NUMDNBS', 'movement', 'SIGNAL', 'BACKGROUND']
        self.phaseOption = ['RHO', 'BIC', 'FIT', 'ESR', 'accGRR', 'LOADINGDNB', 'SNR', 'lag', 'runon']


    def getIntensityAllFov(self, inDir, cycle):
        qcList = glob.glob(os.path.join(inDir, "C*R*.QC.txt"))
        for qc in qcList:
            fov = os.path.basename(qc).split('.')[0]
            if fov not in self.statData:
                self.statData[fov] = {"fov": fov}

            self.getIntensitySingleFov(qc, self.statData[fov], cycle)
        
        return self.statData

    def getPhaseValueAllFov(self, inDir, cycle):
        qcList = glob.glob(os.path.join(inDir, "C*R*.QC.txt"))
        for qc in qcList:
            fov = os.path.basename(qc).split('.')[0]
            if fov not in self.statData:
                self.statData[fov] = {"fov": fov}
            self.getPhaseValueSingleFov(qc, self.statData[fov], cycle)

        return self.statData

    def getIntensitySingleFov(self, qcFile, dataDict, cycle):
        fhIn = open(qcFile, 'r')
        infoLine = 1
        while infoLine:
            infoLine = fhIn.readline()
            valueLine = fhIn.readline()

            if not infoLine:
                break

            info = infoLine.strip('#').split()
            try:
                if info[0] in self.intenOption:
                    valueLine = valueLine.strip().split()
                    if len(valueLine) == 1:
                        dataDict[info[0]] = int(float(valueLine[0]))
                    elif info[0] == 'movement':
                        # mov = round((float(valueLine[(cycle-1)*8])**2 + float(valueLine[(cycle-1)*8+1])**2)**0.5, 2)
                        # dataDict[info[0]] = mov
                        dataDict['OFFSET_X'] = round(math.fabs(float(valueLine[(cycle-1)*8])), 2)
                        dataDict['OFFSET_Y'] = round(math.fabs(float(valueLine[(cycle-1)*8+1])), 2)
                        continue
                    else:
                        dataDict[info[0]+'_A'], dataDict[info[0]+'_C'], dataDict[info[0]+'_G'], \
                                dataDict[info[0]+'_T'] = map(lambda x: int(float(x)), (valueLine[(cycle-1)*4: cycle*4]))
         
            except Exception as e:
                continue

        fhIn.close()

    def getPhaseValueSingleFov(self, qcFile, dataDict, cycle):
        ########### Open and read input file
        fhIn = open(qcFile, 'r')
        infoLine = 1
        while infoLine:
            infoLine = fhIn.readline()
            valueLine = fhIn.readline()
            if not infoLine:
                break

            info = infoLine.strip('#').split()
            try:
                if info[0] in self.phaseOption:
                    valueLine = valueLine.strip().split()
                    if info[0] == 'RHO':
                        dataDict[info[0]+'_A'], dataDict[info[0]+'_C'], dataDict[info[0]+'_G'], \
                                dataDict[info[0]+'_T'] = map(lambda x: int(float(x)), (valueLine[(cycle-1)*4: cycle*4]))
                    elif info[0] == 'SNR':
                        dataDict[info[0]+'_A'], dataDict[info[0]+'_C'], dataDict[info[0]+'_G'], \
                                dataDict[info[0]+'_T'] = map(lambda x: round(float(x), 2), (valueLine[(cycle-1)*4: cycle*4]))
                    elif info[0] in ['lag','runon']:
                        dataDict[info[0]+'_A'], dataDict[info[0]+'_C'], dataDict[info[0]+'_G'], \
                                dataDict[info[0]+'_T'] = map(lambda x: round(float(x), 4), (valueLine[(cycle-1)*4: cycle*4]))
                    elif info[0] == 'accGRR':
                        dataDict['ACC_GRR'] = round(float(valueLine[cycle-1]), 4)
                    else:
                        dataDict[info[0]] = round(float(valueLine[cycle-1]), 4)
            except Exception as e:
                # raise e
                continue
        fhIn.close()
        
        if 'NUMDNBS' in dataDict and 'ESR' in dataDict:
            dataDict['LOADINGDNB'] = int(float(dataDict['NUMDNBS']) * float(dataDict['ESR']))
   
class callCollection(object):
    """docstring for callCollection"""
    
    def __init__(self, statData, onlyInten=True):
        self.reportPara = {}
        self.statData = statData

        if onlyInten:
            self.option = [
                           'NUMDNBS',
                           'OFFSET_X',
                           'OFFSET_Y',
                           'SIGNAL_A',
                           'SIGNAL_C',
                           'SIGNAL_G',
                           'SIGNAL_T',
                           'BACKGROUND_A',
                           'BACKGROUND_C',
                           'BACKGROUND_G',
                           'BACKGROUND_T',
                           ]
        else:
            self.option = [
                           'NUMDNBS',
                           'OFFSET_X',
                           'OFFSET_Y',
                           'SIGNAL_A',
                           'SIGNAL_C',
                           'SIGNAL_G',
                           'SIGNAL_T',
                           'BACKGROUND_A',
                           'BACKGROUND_C',
                           'BACKGROUND_G',
                           'BACKGROUND_T',
                           'lag_A',
                           'lag_C',
                           'lag_G',
                           'lag_T',
                           'runon_A',
                           'runon_C',
                           'runon_G',
                           'runon_T',
                           'RHO_A',
                           'RHO_C',
                           'RHO_G',
                           'RHO_T',
                           'SNR_A',
                           'SNR_C',
                           'SNR_G',
                           'SNR_T',
                           'BIC',
                           'FIT',
                           'ESR',
                           'ACC_GRR',
                           'LOADINGDNB'
                           ]

    def createPara(self, cycle):
        fovData = []
        for k in sorted(self.statData.keys()):
            tmpList = [k]+[self.statData[k][v] for v in self.option if v in self.statData[k]]
            fovData.append(tmpList)
        fovData = {'C%03d' % cycle: fovData}
        self.reportPara['fovData'] = json.dumps(fovData, sort_keys=True)
        self.reportPara['option'] = str(self.option)

    def setPara(self, fovData, option, limit):
        self.reportPara['fovData'] = json.dumps(fovData, sort_keys=True)
        self.reportPara['option'] = json.dumps(option)
       
    def createReport(self, htmlFile):
        templateReport = os.path.join(os.path.dirname(sys.argv[0]), 'templates', 'singleCycleHeatmap.html')
        #self.createPara(cycle)
        self.transformReport(templateReport, htmlFile, self.reportPara)

    def transformReport(self, templateReport, newReport, jsonData):
        fhIn = open(templateReport, 'r')
        template = fhIn.read()
        fhIn.close()
        
        allVar = ['fovData', 'option']
        # Construct a string template and replace json data
        newStr = string.Template(template)
        newStr = newStr.safe_substitute(jsonData)

        notFoundDict = dict.fromkeys([k for k in allVar if k not in jsonData], 'null')
        newStr = string.Template(newStr)
        newStr = newStr.safe_substitute(notFoundDict)

        fhOut = open(newReport, 'w')
        fhOut.write(newStr)
        fhOut.close()


##########################################################################
############################  BEGIN Function  ############################
##########################################################################
def geneOnlyIntenHeatmap(inputDir, cycle, filename):
    statData = {}
    pd = prepraeData(statData)
    # pd.getPhaseValueAllFov(inputDir, int(cycle))
    statData = pd.getIntensityAllFov(inputDir, int(cycle))

    cc = callCollection(statData, onlyInten=True)
    cc.createPara(cycle)
    cc.createReport(filename)

def geneAllInfoHeatmap(inputDir1, cycle1, inputDir2, cycle2, filename):
    statData = {}
    pd = prepraeData(statData)
    pd.getIntensityAllFov(inputDir1, int(cycle1))
    statData = pd.getPhaseValueAllFov(inputDir2, int(cycle2))

    cc = callCollection(statData, onlyInten=False)
    cc.createPara(cycle2)
    cc.createReport(filename)

    return statData

def dumpDnbLoad2Csv(statData, outfile):
    dnbDict = {}
    grrDict = {}

    for fov in statData:
        if 'LOADINGDNB' in statData[fov] and 'ESR' in statData[fov]:
            dnbDict[fov] = statData[fov]['LOADINGDNB']
            grrDict[fov] = statData[fov]['ESR']

    cMax = 0
    rMax = 0
    for fov in dnbDict.keys():
        c = int(fov[1:4])
        r = int(fov[5:])
        if c > cMax:
            cMax = c
        if r > rMax:
            rMax = r
    # print cMax, rMax
    fhOut = open(outfile, 'w')
    outString = 'LOADINGDNB,' + ','.join(str(i) for i in xrange(1, rMax+1)) + '\n'
    for row in xrange(cMax):
        outString += str(row + 1) + ','
        for col in xrange(rMax):
            fov = 'C%03dR%03d' % (row+1, col+1)
            if fov in dnbDict:
                outString += str(dnbDict[fov]) + ','
            else:
                outString += str(0) + ','
        outString += '\n'

    outString += '\nESR,' + ','.join(str(i) for i in xrange(1, rMax+1)) + '\n'
    for row in xrange(cMax):
        outString += str(row + 1) + ','
        for col in xrange(rMax):
            fov = 'C%03dR%03d' % (row+1, col+1)
            if fov in grrDict:
                outString += str(grrDict[fov]) + ','
            else:
                outString += str(0) + ','
        outString += '\n'

    fhOut.write(outString)
    fhOut.close()

######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 3:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (inputDir, cycle, report) = args

    ############################# Main Body #############################
    statData = {}
    pd = prepraeData(statData)
    pd.getPhaseValueAllFov(inputDir, int(cycle))
    statData = pd.getIntensityAllFov(inputDir, int(cycle))

    cc = callCollection(statData, onlyInten=False)
    cc.createPara(cycle)
    cc.createReport(report)

#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
